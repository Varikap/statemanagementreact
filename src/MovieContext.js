import React, {useState, createContext} from 'react';

export const MovieContext = createContext();

export const MovieProvider = (props) => {
    const [movies, setMovies] = useState([
        {
            name: 'American History X',
            price: '$10',
            id:23131312
        },
        {
            name: 'Memento',
            price: '$20',
            id:23
        },
        {
            name: 'Inception',
            price: '$30',
            id:5632
        }
    ]);
    return (
        // prop must be called value
        <MovieContext.Provider value={[movies, setMovies]}>
            {props.children}
        </MovieContext.Provider>
    );
}