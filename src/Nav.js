import React, { useState, useContext } from 'react';
import {MovieContext} from './MovieContext';

const Nav = (props) => {
    const [movies, setMovies] = useContext(MovieContext);
    return (
        <div>
            <h3>Varikap</h3>
            <p>List of Movies: {movies.length}</p>
        </div>
    );
}

export default Nav;